<title>Fawdaw OS</title>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<link href="/css/bootstrap.min.css" rel="stylesheet">
<link href="/css/style.css" rel="stylesheet">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<nav class="navbar navbar-default navbar-fixed-bottom <?php echo $start_menu_color; ?>" role="navigation">
				<div class="navbar-header">
					<ul class="nav navbar-nav">
						<li class="dropdown">
							 <a href="" class="dropdown-togglenavbar-brand" data-toggle="dropdown"><b>Start Menu</b></a>
							<ul class="dropdown-menu">
								<?php include("/start_menu.php"); ?>
							</ul>
						</li>
					</ul>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							 <a href="" class="dropdown-togglenavbar-brand" data-toggle="dropdown"><b><?php echo $date; ?></b></a>
							<ul class="dropdown-menu">
								<?php include("/application/calendar.php"); ?>
							</ul>
						</li>
						<li class="divider-vertical"></li>
					</ul>
				</div>
			</nav>
		</div>
	</div>
</div>
<div class="container-fluid">
	<br>
	<a class="btn <?php echo $desktop_icon_color; ?> btn-lg" href="/application/gallery/" role="button">
		<span class="glyphicon glyphicon-picture" aria-hidden="true"></span>
		<br>
		Photo Gallery
	</a>
</div>
<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/scripts.js"></script>
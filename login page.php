<title>Fawdaw OS</title>
<style type="text/css">
@media (max-width: 767px) {
    .navbar-collapse .nav > .divider-vertical {
        display: none;
     }
}
			.panel-body {
			  padding-top: 0px;
			  padding-bottom: 10px;
			  background-color: #fffff;
			}
			.form-signin {
			  max-width: 380px;
			  padding: 15px;
			  margin: 0 auto;
			}
			.form-signin .form-signin-heading,
			.form-signin .checkbox {
			  margin-bottom: 10px;
			}
			.form-signin .checkbox {
			  font-weight: normal;
			}
			.form-signin .form-control {
			  position: relative;
			  height: auto;
			  -webkit-box-sizing: border-box;
				 -moz-box-sizing: border-box;
					  box-sizing: border-box;
			  padding: 10px;
			  font-size: 16px;
			}
			.form-signin .form-control:focus {
			  z-index: 2;
			}
			.center {
			  position: relative;
			  top: 100%;
			  transform: translateY(25%);
			}
			.animation {
			  -webkit-animation-delay: 900ms;
			  animation-delay: 900ms;
			  -moz-animation-delay: 900ms;
			  -ms-animation-delay: 900ms;
			  -o-animation-delay: 900ms;
			}
</style>
	<body>
		<br><br><br><br><br><br><br><br>
		<div class="container">
			<form class="form-signin animated fadeInDown" role="form">
				<div class="panel panel-danger" style="border-radius: 0px; !important">
					<div class="panel-body">
						<h3 class="form-signin-heading" align="center">WebOS Login</h3>
						<label for="user">Username</label>
						<input type="username" id="user" name="user" class="form-control" autocomplete="off" required autofocus style="border-radius: 0px; !important">
						<br>
						<label for="password">Password</label>
						<input type="password" id="password" name="password" class="form-control" autocomplete="off" required style="border-radius: 0px; !important">
						<br>
						<button class="btn btn-primary pull-left" type="submit" style="border-radius: 0px; !important">Login</button>
						<br><br>
						<small>Guest Account avaliable! User and pass: guest</small>
					</div>
				</div>
			</form>
		</div>
	</body>
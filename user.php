<title><?php echo $user; ?> | Fawdaw OS</title>
<br>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<nav class="navbar navbar-default navbar-fixed-bottom <?php echo $start_menu_color; ?>" role="navigation">
				<div class="navbar-header">
					<ul class="nav navbar-nav">
						<li class="dropdown">
							 <a href="" class="dropdown-togglenavbar-brand" data-toggle="dropdown"><b>Start Menu</b></a>
							<ul class="dropdown-menu">
								<?php include("/start_menu.php"); ?>
							</ul>
						</li>
					</ul>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="dropdown active">
							 <a href="" class="dropdown-togglenavbar-brand" data-toggle="dropdown">Account Info <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span></a>
							<ul class="dropdown-menu">
								<li>
									<a href="/desktop">Close</a>
								</li>
							</ul>
						</li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							 <a href="" class="dropdown-togglenavbar-brand" data-toggle="dropdown"><b><span align="rigth" class="glyphicon glyphicon-signal" aria-hidden="true"></span></b></a>
							<ul class="dropdown-menu">
								<?php include("../application/network.php"); ?>
								<li><a href="">WebOS 40Gbps <span align="rigth" class="glyphicon glyphicon-signal" aria-hidden="true"></span></a></li>
							</ul>
						</li>
						<li class="dropdown">
							 <a href="" class="dropdown-togglenavbar-brand" data-toggle="dropdown"><b><?php echo $date; ?></b></a>
							<ul class="dropdown-menu">
								<?php include("../application/calendar.php"); ?>
							</ul>
						</li>
						<li class="divider-vertical"></li>
					</ul>
				</div>
				
			</nav>
		</div>
	</div>
</div>
<div class="container">
	<div class="panel panel-default">
		<div class="panel-body">
			Username: <br>
			Account Name: <br>
			User ID: <br>
			Date: <br>
			Wallpaper: <br>
			Start Menu Color: <br>
			Web Browser Color: <br>
			Avatar: <br>
		</div>
	</div>
</div>
